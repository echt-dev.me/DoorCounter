import RPi.GPIO as GPIO  # Import GPIO library
import time
import os

GPIO.setmode(GPIO.BCM)  # Set GPIO pin numbering

TRIG = 23  # Associate pin 23 to TRIG
ECHO = 24  # Associate pin 24 to ECHO

print "Distance measurement in progress"

GPIO.setup(TRIG, GPIO.OUT)  # Set pin as GPIO out
GPIO.setup(ECHO, GPIO.IN)  # Set pin as GPIO in

DistanceArray = []
DoorOpen = False
FistStart = True
IdelDistance = 0


def getDistance():
    GPIO.output(TRIG, False)  # Set TRIG as LOW
    time.sleep(0.75)  # Delay of 2 seconds

    GPIO.output(TRIG, True)  # Set TRIG as HIGH
    time.sleep(0.00001)  # Delay of 0.00001 seconds
    GPIO.output(TRIG, False)  # Set TRIG as LOW

    while GPIO.input(ECHO) == 0:  # Check whether the ECHO is LOW
        pulse_start = time.time()  # Saves the last known time of LOW pulse

    while GPIO.input(ECHO) == 1:  # Check whether the ECHO is HIGH
        pulse_end = time.time()  # Saves the last known time of HIGH pulse

    pulse_duration = pulse_end - pulse_start  # Get pulse duration to a variable

    # Multiply pulse duration by 17150 to get distance
    distance = pulse_duration * 17150
    distance = round(distance, 2)  # Round to two decimal points
    return distance


while True:
    # Get Distance
    distance = getDistance()
    #print distance

    # Set Idle Distance for first time
    if FistStart == True:
        FistStart = False
        time.sleep(2)  # Delay of 2 seconds
        # Get second option
        distance = getDistance()
        if distance > 500:
            IdelDistance = 500
        else:
            IdelDistance = distance
        os.system("curl http://dev.echt.me?distance=" + format(distance))
        minDistance = IdelDistance - (IdelDistance * 0.1)
        maxDistance = IdelDistance - (IdelDistance * 0.25)
        print "Idel-Distance: ", IdelDistance

    # Door Opened
    if distance < maxDistance:
        if len(DistanceArray) == 2:
            print "Door open, Distance:", distance - 0.5, "cm"
            os.system("curl http://dev.echt.me?access=foobar")
            DistanceArray.append(distance)
            time.sleep(1)
        else:
            DistanceArray.append(distance)
    else:
        DistanceArray = []
